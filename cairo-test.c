// Started with https://github.com/xuzhezhaozhao/Cairo-Tutorial/blob/master/05%20GTK/cairo-gtk.c

#include <cairo.h>
#include <gtk/gtk.h>

/**
 * Drawing images with an alpha value
 */
static void draw_image(
    cairo_t *cr, char *src_png, int x, int y, double alpha
) {
    cairo_surface_t *image
        = cairo_image_surface_create_from_png(src_png);
    cairo_set_source_surface(cr, image, x, y);
    cairo_paint_with_alpha(cr, alpha);
    cairo_surface_destroy(image);
}

/**
 * Draws a line directly on the main surface
 */
static void draw_some_line(cairo_t *cr) {
    cairo_set_source_rgba(cr, 1.0, 0, 0, 0.7);

    cairo_move_to(cr, 25, 50);
    cairo_line_to(cr, 50, 60);
    cairo_line_to(cr, 50, 100);
    cairo_line_to(cr, 200, 170);
    cairo_line_to(cr, 400, 250);

    cairo_set_line_width(cr, 4);
    cairo_stroke(cr);
}

/**
 * Tests the idea of drawing to an intermediate surface before copying
 * that surface onto the main one (cr).
 */
static void draw_some_line_on_surface_first(cairo_t *cr) {

    // create a buffer surface to draw on first before copying to cr
    cairo_surface_t *surface = cairo_image_surface_create(
        CAIRO_FORMAT_ARGB32, 500, 500
    );

    // create a cairo context to do the drawing on surface
    cairo_t *tile = cairo_create(surface);

    // do whatever drawing
    cairo_set_source_rgba(tile, 0, 0, 1.0, 0.7);

    cairo_move_to(tile, 250, 100);
    cairo_line_to(tile, 50, 160);
    cairo_line_to(tile, 500, 100);
    cairo_line_to(tile, 200, 270);
    cairo_line_to(tile, 400, 450);

    cairo_set_line_width(tile, 10);
    cairo_stroke(tile);

    // done with cairo context
    cairo_destroy(tile);

    // copy surface to main surface
    cairo_set_source_surface(cr, surface, 0, 0);
    // could use cairo_paint_with_alpha instead
    cairo_paint(cr);

    cairo_surface_destroy(surface);
}


static gboolean on_draw_event(
    GtkWidget *widget, cairo_t *cr, gpointer user_data
) {
    draw_image(cr, "65068.png", 0, 0, 1);
    draw_image(cr, "65068.png", 100, 100, .5);

    draw_some_line(cr);
    draw_some_line_on_surface_first(cr);
}

int main(int argc, char *argv[]) {
    GtkWidget *window;
    GtkWidget *darea;

    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    darea = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(window), darea);

    g_signal_connect(G_OBJECT(darea), "draw",
        G_CALLBACK(on_draw_event), NULL);
    g_signal_connect(window, "destroy",
        G_CALLBACK(gtk_main_quit), NULL);

    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 90);
    gtk_window_set_title(GTK_WINDOW(window), "GTK window");

    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
