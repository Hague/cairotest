# Cairo Test

Testing out Cairo: composing surfaces, images, lines.

Nothing too fancy. More comprehensive range of samples in the [official
docs][cairo-samples].

[cairo-samples]: https://www.cairographics.org/samples/
